package com.zl;

import com.zl.bean.User;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class Start {
    public static void main(String[] args) {
        ApplicationContext act = SpringApplication.run(Start.class,args);
        User user = act.getBean(User.class);
        System.out.println(user);
    }
}
