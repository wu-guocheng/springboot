package com.zl.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

//@RestController  //返回的是请求体，而不是路径
@Controller  //返回路径
@RequestMapping("/test")
public class TestController {
    @RequestMapping("/hello")
    public String test(Model model) {
        model.addAttribute("hello", "welcome Mr.Wu");
        return "/demo";
    }
}
