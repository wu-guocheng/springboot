package com.zl.dao;

import com.zl.bean.User;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

//@Mapper  //注意别忘
public interface UserDao extends JpaRepository<User,Integer> {
    List<User> findAll2();  //为什么使用jpa时不能存在???不满足jpa规范。jpa与mybatis分开来
}
