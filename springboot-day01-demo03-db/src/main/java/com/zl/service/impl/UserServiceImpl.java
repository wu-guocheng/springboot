package com.zl.service.impl;

import com.zl.bean.User;
import com.zl.dao.UserDao;
import com.zl.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserDao userDao;
    @Autowired
    private RedisTemplate redisTemplate;
    @Override
    public List<User> findAll() {
        List<User> users = (List<User>) redisTemplate.boundValueOps("users").get();
        if (users != null) {
            System.out.println("数据来自redis");
            return users;
        }
        users = userDao.findAll();
        System.out.println("数据来自mysql");
        redisTemplate.boundValueOps("users").set(users);
        return users;
    }

    @Override
    public User findById(int id) {
        System.out.println(id);
        return userDao.findById(id).get();
    }

    @Override
    public void saveUser(User user) {
        userDao.save(user);
    }

    @Override
    public void updateUser(User user) {
        userDao.save(user);
    }

    @Override
    public void deleteUserById(Integer id) {
        userDao.deleteById(id);
    }
}
