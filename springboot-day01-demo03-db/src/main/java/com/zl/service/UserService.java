package com.zl.service;

import com.zl.bean.User;

import java.util.List;

public interface UserService {
    List<User> findAll();
    User findById(int id);

    void saveUser(User user);

    void updateUser(User user);

    void deleteUserById(Integer id);
}
