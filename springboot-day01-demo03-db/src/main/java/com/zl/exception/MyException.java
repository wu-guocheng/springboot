package com.zl.exception;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@ControllerAdvice
public class MyException {
    @ExceptionHandler(Exception.class)
    @ResponseBody  //响应体接受返回的json
    public String method(Exception e) {
        System.out.println("发生异常");
        e.printStackTrace();
        return e.toString();
    }
}
