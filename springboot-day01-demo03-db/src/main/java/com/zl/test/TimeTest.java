package com.zl.test;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class TimeTest {
    //@Scheduled(fixedDelay = 3000)  //延时，大家一起来
    //@Scheduled(fixedRate = 3000)  //频率，谁大谁来
    @Scheduled(cron = "0/2 * * * * ?")  //cron都是按照整点来运行的，比如0秒开始，2秒一次，他会在0,2,4…秒运行，如果那个时间点还没运行结束，那么就会跳过这次任务。
    public void method() {
        System.out.println("method");
        try {
            //Thread.sleep(6000);  //6+秒的下一个运行间隔是8秒后
            //Thread.sleep(5000);  //5+秒的下一个运行间隔是6秒后
            Thread.sleep(3000);  //4
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(new Date().toLocaleString());
    }
}
