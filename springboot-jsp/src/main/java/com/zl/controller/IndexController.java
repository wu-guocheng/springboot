package com.zl.controller;

import com.zl.exception.MyException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class IndexController {
    @GetMapping("/")
    public String index(Model model){
        System.out.println("index");
        if (true) {
            throw new MyException(123456,"我的异常");
        }
        model.addAttribute("name", "ywj");
        return "index";
    }
}