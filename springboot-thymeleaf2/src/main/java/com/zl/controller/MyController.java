package com.zl.controller;

import com.zl.bean.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.*;

@Controller
@RequestMapping("/test")
public class MyController {
    @RequestMapping("/hello")
    public String test(Model model) {
        model.addAttribute("hello", "<span style=\"color: red\">hello thymeleaf</span>");
        //集合数据,通过构造方法创建对象
        List<User> users = new ArrayList<User>();
        users.add(new User(1,"张三","深圳"));
        users.add(new User(2,"李四","北京"));
        users.add(new User(3,"王五","武汉"));
        model.addAttribute("users",users);

        model.addAttribute("date", new Date());

        model.addAttribute("age", 16);
        //Map定义
        Map<String,Object> dataMap = new HashMap<String,Object>();
        dataMap.put("No","123");
        dataMap.put("address","深圳");
        model.addAttribute("dataMap",dataMap);
        //字符判断和处理
        model.addAttribute("name","spec_fbb");
        //添加一个方法地址给前端调用
        model.addAttribute("url", "/test/add");
        return "/demo";
    }
    /**
     * 接收前端数据,给前端点击超链接来调用,但是我后台要传递这个方法的url地址到域对象给前端
     * @return
     */
    @RequestMapping("/add")
    public String add(String name,String address) {
        System.out.println(name+"  住址是 "+address);
        return "redirect:http://www.baidu.com";
    }
}
