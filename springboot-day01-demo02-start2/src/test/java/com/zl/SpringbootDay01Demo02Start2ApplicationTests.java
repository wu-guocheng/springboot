package com.zl;


public class SpringbootDay01Demo02Start2ApplicationTests {
    public static void method(char[] chars) {
        chars[0] = 'a';
    }
    public static void method2(int[] arr) {
        arr[0]=1;
    }
    public static void method3(String str) {
        str = "abc";
    }

    public static void main(String[] args) {
        char[] chars = {'d', 'e', 'f'};
        int[] arr = {2, 5, 6};
        String str = "sdf";
        method(chars);
        System.out.println(chars); //aef  变

        method2(arr);
        System.out.println(arr[0]); //1   变

        method3(str);
        System.out.println(str);  //sdf  没变
    }
}
