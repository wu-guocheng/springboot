package com.zl;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootDay01Demo02Start2Application {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootDay01Demo02Start2Application.class, args);
    }

}
